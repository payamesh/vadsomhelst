import React from 'react';
import { mount, shallow } from 'enzyme';
import App from '../components/App';

test('should render <App /> without user', () => {
 const wrapper = mount(<App user="dansdo@gmail.com" />);
wrapper.instance().logout();
 expect(wrapper.state('user')).toBe('');
});

test('should render <App /> with user', () => {
    const wrapper = mount(<App user="dansdo@gmail.com" />);
 expect(wrapper.state('user')).toBe('dansdo@gmail.com');

});

test('call the internal method loginSuccessful', () => {
    const wrapper = mount(<App user="hello@gmail.com" />);
    wrapper.instance().loginSuccessful('hejhej@gmail.com');
    expect(wrapper.state('user')).toBe('hejhej@gmail.com')

});

test('call the internal method logout', () => {
    const wrapper = mount(<App user="hello@gmail.com" />);

    wrapper.instance().loginSuccessful('hejhej@gmail.com');
    wrapper.instance().logout();
    expect(wrapper.state('user')).toBe('')    
});

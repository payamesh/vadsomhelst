import * as localStorageUtils from '../utils/localStorage';

beforeEach(() =>{
  localStorage.clear();
});

afterEach(() =>{
  localStorage.clear();
});

test('should get the user from localStorage', () => {
  localStorageUtils.saveUserToLocalStorage('payam')
expect(localStorageUtils.getUserFromLocalStorage()).toBe('payam')

});

test('should get empty user from localStorage', () => {
  localStorageUtils.removeUserFromLocalStorage();
expect(localStorageUtils.getUserFromLocalStorage()).toBe('')
});
